// Defines package
package ch.tbz;

// Imports libraries
import static java.lang.System.*;   // System-IO Library for abbriviation purpose to static functions
import static java.lang.Math.*;     // Mathematic Library for abbriviation purpose to static functions
import java.util.*;                 // Random is part of this library

// Imports Input functions
import static ch.tbz.lib.Input.*;       // All the functions can be used now!

//Every program must be placed in a class ...
public class I1 {

    // Our main function which runs the program12
    public static void main(String[] args ) {
        double dbIX = inputDouble("Geben Sie die erste Zahl ein: ");
        double dbIY = inputDouble("Geben Sie die zweite Zahl ein: ");

        double summe = dbIX + dbIY;
        out.println("Die Summe ist: " + summe);
        if( summe < 0)
        {
            out.println("======================");
        }


        double d = dbIX - dbIY;
        out.println("Die Subtraktion ist: " + d);
        if( d < 0)
        {
            out.println("======================");
        }

        double e = dbIX * dbIY;
        out.println("Die Multiplikation ist: " + e);

        if( e < 0)
        {
            out.println("======================");
        }

        double f = dbIX / dbIY;
        out.println("Die Division ist: " + f);
        if( f < 0)
        {
            out.println("======================");
        }
        double g = dbIX % dbIY;
        out.println("Der Rest ist: " + g);
        if( g < 0)
        {
            out.println("======================");
        }
        double dbIZ = inputDouble("Geben Sie die dritte Zahl ein die mit der Summe der ersten zwei Zahlen verr" +
                "echnet werden sollte: ");

        double Kettenrechnung = summe + dbIZ;
        out.println("Die Summe der Kettenrechnung ist " + Kettenrechnung);
        if( Kettenrechnung < 0)
        {
            out.println("======================");
        }
    }
}